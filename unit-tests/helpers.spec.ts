import { IDisposable } from "../src";

export interface ITestResource extends IDisposable {
    readonly disposed: boolean;
}

export function createDisposable(): ITestResource {
    let disposed = false;

    return {
        get disposed(): boolean {
            return disposed;
        },
        dispose: () => {
            disposed = true;
        }
    };
}
