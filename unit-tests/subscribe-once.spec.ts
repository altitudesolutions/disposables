import { BehaviorSubject, Subject } from "rxjs";

import { Disposables } from "../src";

describe("disposables.subscribeOnce()", () => {
    it("Should not be added to disposables when processed synchronously", () => {
        let value = 0;
        const d = new Disposables();
        const trigger = new BehaviorSubject(1);
        const sub = d.subscribeOnce(trigger, v => {
            value = v;
        });
        expect(sub.closed)
            .withContext("Did not close subscription")
            .toBe(true);
        expect(d.disposeOf(sub))
            .withContext("Added subscription to interal disposables collection")
            .toBe(false);
        expect(value)
            .withContext("Did not fire event")
            .toBe(1);
    });

    it("Should complete when processed synchronously", () => {
        let completed = false;
        const d = new Disposables();
        const trigger = new BehaviorSubject(true);
        d.subscribeOnce(trigger, {
            complete: () => {
                completed = true;
            }
        });
        expect(completed)
            .withContext("Did not complete")
            .toBe(true);
    });

    it("Should be added to disposables when processed asynchronously", () => {
        let value = 0;
        const d = new Disposables();
        const trigger = new Subject<number>();
        const sub = d.subscribeOnce(trigger, v => {
            value = v;
        });
        expect(sub.closed)
            .withContext("Prematurely closed subscription")
            .toBe(false);
        expect(d.disposeOf(sub))
            .withContext(
                "Did not add subscription to interal disposables collection"
            )
            .toBe(true);
        trigger.next(1);
        expect(value)
            .withContext("Should not have fired event")
            .toBe(0);
    });
    it("Should complete when processed asynchronously", () => {
        let completed = false;
        const d = new Disposables();
        const trigger = new Subject<boolean>();
        d.subscribeOnce(trigger, {
            complete: () => {
                completed = true;
            }
        });
        expect(completed)
            .withContext("Completed prematurely")
            .toBe(false);
        trigger.next(true);
        expect(completed)
            .withContext("Did not complete")
            .toBe(true);
    });
    it("Should unsubscribe when source completes synchronously without emitting", () => {
        let completed = false;
        let emitted = false;
        const d = new Disposables();
        const trigger = new Subject<boolean>();
        expect(emitted)
            .withContext("Emitted")
            .toBe(false);
        expect(completed)
            .withContext("Completed")
            .toBe(false);

        trigger.complete();
        const sub = d.subscribeOnce(
            trigger,
            () => {
                emitted = true;
            },
            undefined,
            () => {
                completed = true;
            }
        );
        expect(emitted)
            .withContext("Emitted")
            .toBe(false);
        expect(sub.closed)
            .withContext("Failed to close")
            .toBe(true);
        expect(completed)
            .withContext("Failed to complete")
            .toBe(true);
    });
    it("Should unsubscribe when source completes asynchronously without emitting", () => {
        let completed = false;
        let emitted = false;
        const d = new Disposables();
        const trigger = new Subject<boolean>();
        const sub = d.subscribeOnce(
            trigger,
            () => {
                emitted = true;
            },
            undefined,
            () => {
                completed = true;
            }
        );
        expect(emitted)
            .withContext("Emitted")
            .toBe(false);
        expect(completed)
            .withContext("Completed")
            .toBe(false);
        expect(sub.closed)
            .withContext("Closed prematurely")
            .toBe(false);
        trigger.complete();
        expect(sub.closed)
            .withContext("Failed to close")
            .toBe(true);
        expect(completed)
            .withContext("Failed to complete")
            .toBe(true);
        expect(emitted)
            .withContext("Emitted")
            .toBe(false);
    });
    it("Should unsubscribe after dispose invoked before source has emitted.", () => {
        let completed = false;
        let emitted = false;
        const d = new Disposables();
        const trigger = new Subject<boolean>();
        const sub = d.subscribeOnce(
            trigger,
            () => {
                emitted = true;
            },
            undefined,
            () => {
                completed = true;
            }
        );
        expect(emitted)
            .withContext("Emitted")
            .toBe(false);
        expect(completed)
            .withContext("Completed")
            .toBe(false);
        expect(sub.closed)
            .withContext("Closed prematurely")
            .toBe(false);
        d.dispose();
        trigger.complete();
        expect(sub.closed)
            .withContext("Failed to close")
            .toBe(true);
        expect(completed)
            .withContext("Failed to complete")
            .toBe(false);
        expect(emitted)
            .withContext("Emitted")
            .toBe(false);
    });
});
