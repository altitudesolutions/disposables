import { Disposables } from "../src";

describe("disposables.getContext()", () => {
    it("Should be false after dispose has been invoked", () => {
        const d = new Disposables();
        const context = d.getContext();

        expect(context.isDisposed)
            .withContext("Does not reflect active state")
            .toBe(false);

        d.dispose();

        expect(context.isDisposed)
            .withContext("Does not reflect disposed state")
            .toBe(true);
    });

    it("Should issue a new context after dispose has been invoked", () => {
        const d = new Disposables();
        const context1 = d.getContext();
        d.dispose();
        const context2 = d.getContext();

        expect(context1.isDisposed)
            .withContext("Initial context is not disposed")
            .toBe(true);
        expect(context2.isDisposed)
            .withContext("Active constext is disposed")
            .toBe(false);
        expect(context1 === context2)
            .withContext("Both context references are the same")
            .toBe(false);
    });
});
