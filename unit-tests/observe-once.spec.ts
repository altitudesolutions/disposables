import { Subject } from "rxjs";

import { Disposables } from "../src";

/* tslint:disable:no-magic-numbers*/

describe("disposables.observeOnce()", () => {
    it("Should be multicast", () => {
        let proxiedEventCount = 0;
        const d = new Disposables();
        const subject = new Subject<number>();
        const obs = d.observeOnce(subject);
        // Because observeOnce does not itself subscribe to the specified observable,
        // we must initially subscribe before invoking `next` on the subject so as to
        // capture and mulitcast the emission.
        obs.subscribe();
        subject.next(1);
        for (let i = 2; i < 5; i += 1) {
            obs.subscribe(val => {
                proxiedEventCount += 1;
                expect(val)
                    .withContext("Unexpected multicase value")
                    .toBe(1);
            });
        }
        expect(proxiedEventCount)
            .withContext("Did not fire expected number of proxied events")
            .toBe(3);
    });

    it("Should cancel without emitting if disposed", () => {
        let emitted = false;
        const d = new Disposables();
        const subject = new Subject<void>();

        d.observeOnce(subject).subscribe(() => {
            emitted = true;
        });
        d.dispose();
        subject.next();

        expect(emitted)
            .withContext("Emitted after dispose")
            .toBe(false);
    });
});
