import { Disposables } from "../src";

import { createDisposable, ITestResource } from "./helpers.spec";

/* tslint:disable:no-magic-numbers*/

describe("disposables.disposeOf()", () => {
    it("Should dispose of resources and remove them from internal collection using the target object as reference", () => {
        const resources: ITestResource[] = [];
        for (let i = 0; i < 4; i += 1) {
            resources.push(createDisposable());
        }
        const d = new Disposables();
        resources.forEach(r => {
            d.autoDispose(r);
        });

        function removeAndCheck(r: ITestResource): void {
            expect(r.disposed)
                .withContext("Resource prematurely disposed")
                .toBe(false);
            expect(d.disposeOf(r))
                .withContext("Resource not found")
                .toBe(true);
            expect(r.disposed)
                .withContext("Resource not disposed")
                .toBe(true);
            expect(d.disposeOf(r))
                .withContext("Resource not removed")
                .toBe(false);
        }

        resources.forEach(r => {
            removeAndCheck(r);
        });
    });

    it("Should dispose of resources and remove them from internal collection using a proxy function as reference", () => {
        const resources: ITestResource[] = [];
        for (let i = 0; i < 4; i += 1) {
            resources.push(createDisposable());
        }
        const d = new Disposables();
        resources.forEach(r => {
            // tslint:disable-next-line: no-unbound-method
            d.invokeOnDispose(r.dispose);
        });

        function removeAndCheck(r: ITestResource): void {
            expect(r.disposed)
                .withContext("Resource prematurely disposed")
                .toBe(false);
            // tslint:disable-next-line: no-unbound-method
            expect(d.disposeOf(r.dispose))
                .withContext("Resource not found")
                .toBe(true);
            expect(r.disposed)
                .withContext("Resource not disposed")
                .toBe(true);
            expect(d.disposeOf(r))
                .withContext("Resource not removed")
                .toBe(false);
        }

        resources.forEach(r => {
            removeAndCheck(r);
        });
    });
});
