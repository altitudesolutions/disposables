import * as Async from "@altitude/async";
import * as ErrorMessage from "@altitude/error-message";
import {
    Observable,
    PartialObserver,
    Subscription,
    SubscriptionLike
} from "rxjs";
import { shareReplay, take, takeWhile } from "rxjs/operators";

/**
 * An interface to an object which can be disposed.
 */
export interface IDisposable {
    dispose(): void;
}

interface IProxy extends IDisposable {
    readonly proxied: any;
    dispose(): void;
}

/**
 * An interface to an object which manages disposable resources such as RxJS subscriptions and observables, as well as custom disposable resources,
 * but does not allow client code to initiate diposal other than for individual resources which it holds a reference to.
 */
export interface IDisposer {
    /**
     * Registers a subscription for automatic disposal when dispose() has been invoked.
     *
     * @param subscription The subscription to register for disposal.
     */
    addSubscription(subscription: SubscriptionLike): void;
    /**
     * Registers a disposable resource to be automatically disposed of during this instance's dispose sequence.
     *
     * @param resource The resource to register for automatic disposal.
     */
    autoDispose(resource: IDisposable): void;
    /**
     * Registers a disposable resource to be automatically disposed of by proxy during this instance's dispose sequence.
     *
     * The specified proxy function will be invoked when the resource is ready for disposal.
     *
     * @param resource The resource to register for automatic disposal.
     */
    autoDisposeProxy(resource: any, disposeFn: () => void): void;
    /**
     * If the specified resource is currently managed by the instance, removes it from the instance's internal collection
     * and disposes of it.
     *
     * @param resource The resource to dispose of.
     * @returns `true` if the resource was managed by the instance (and therefore disposed), otherwise returns `false`.
     */
    disposeOf(resource: any): boolean;
    /**
     * Gets a reference to the instance's active `DisposableContext` instance.
     *
     * Note that a `DisposableContext` instance becomes stale upon the next invocation of the `dispose()` method.
     * If a fresh context is required following `dispose()`, then `getContext()`must be invoked again.
     */
    getContext(): IDisposableContext;
    /**
     * Registers a function to be invoked upon disposal. Note that this is the equivalent of invoking
     * `autoDisposeProxy(fn, fn)`, and therefore `fn` will be removed from the instance's internal collection
     * following invocation of `dispose()`.
     * @param fn The function to invoke upon disposal.
     */
    invokeOnDispose(fn: () => void): void;
    /**
     * Returns an observable which proxies zero or one events raised by a source observable, and automatically unsubscribes from
     * the source observable when the event has completed or dispose() has been invoked, whichever occurs first.
     * @param observable The source observable to subscribe to.
     * @returns A throw-away Observable which can be subscribed to inline by client code but needn't be unsubscribed from.
     */
    observeOnce<T>(observable: Observable<T>): Observable<T>;
    /**
     * Subscribes to an observable for zero or one events, and automatically unsubscribes when an event has completed or
     * dispose() has been invoked, whichever occurs first.
     *
     * @param observable The observable to subscribe to.
     * @param observer The observer which should handle the observable events.
     * @returns A `Subscription`.
     */
    subscribeOnce<T>(
        observable: Observable<T>,
        observer: PartialObserver<T>
    ): Subscription;
    /**
     * Subscribes to an observable for zero or one events, and automatically unsubscribes when an event has completed or
     * dispose() has been invoked, whichever occurs first.
     *
     * @param observable The observable to subscribe to.
     * @param next A handler for each value emitted by the observable.
     * @param error A optional error handler.
     * @param completed A optional completed handler.
     * @returns A `Subscription`.
     */
    subscribeOnce<T>(
        observable: Observable<T>,
        next: (value: T) => void,
        error?: (error: any) => void,
        completed?: () => void
    ): Subscription;
    /**
     * Subscribes to an observable and automatically unsubscribes from it when dispose() has been invoked.
     *
     * @param observable The observable to subscribe to.
     * @param next A handler for each value emitted by the observable.
     * @param error A optional error handler.
     * @param completed A optional completed handler.
     * @returns A `Subscription`.
     */
    subscribeTo<T>(
        observable: Observable<T>,
        next: (value: T) => void,
        error?: (error: any) => void,
        complete?: () => void
    ): Subscription;
}

/**
 * Represents the context of a `Disposables` instance session.
 */
export interface IDisposableContext {
    /**
     * Returns `true` if the context has been disposed, otherwise returns `false`.
     */
    readonly isDisposed: boolean;
}

interface IInternalContext extends IDisposable {
    readonly shared: IDisposableContext;
    dispose(): void;
}

/**
 * Manages resources such as RxJS subscriptions and observables, as well as custom disposable resources.
 *
 * The `Disposables` class offers a useful pattern for the consistent contextual management of multiple RxJS observables,
 * subscriptions and custom disposable resources. All managed observables will be automatically cancelled when the `dispose`
 * method is invoked, all managed subscriptions unsubscribed from, and all custom resources disposed.
 * This not only eliminates potentially difficult to debug memory leaks, but also ensures that no event handlers are invoked when
 * they are no longer in context (for example, their host component is no longer in scope or has reached the end of its lifecycle).
 */
export class Disposables implements IDisposer {
    private _context: IDisposableContext | undefined;
    private _disposables: IProxy[] | undefined;

    /**
     * Registers a subscription for automatic disposal when dispose() has been invoked.
     *
     * @param subscription The subscription to register for disposal.
     */
    addSubscription(subscription: SubscriptionLike): void {
        this.autoDisposeProxy(subscription, () => {
            if (!subscription.closed) subscription.unsubscribe();
        });
    }

    /**
     * Registers a disposable resource to be automatically disposed of during this instance's dispose sequence.
     * @param resource The resource to register for automatic disposal.
     */
    autoDispose(resource: IDisposable): void {
        if (resource == undefined) {
            throw new Error(ErrorMessage.argumentNull("resource"));
        }
        if (typeof (<any>resource).dispose === "function") {
            this.autoDisposeProxy(resource, () => {
                resource.dispose();
            });
        } else {
            throw new Error(ErrorMessage.argument("resource"));
        }
    }

    /**
     * Registers a disposable resource to be automatically disposed of by proxy during this instance's dispose sequence.
     *
     * The specified proxy function will be invoked when the resource is ready for disposal.
     *
     * @param resource The resource to register for automatic disposal.
     */
    autoDisposeProxy(resource: any, disposeFn: () => void): void {
        if (resource == undefined) {
            throw new Error(ErrorMessage.argumentNull("resource"));
        }
        if (disposeFn == undefined) {
            throw new Error(ErrorMessage.argumentNull("disposeFn"));
        }

        if (this._disposables == undefined) {
            this._disposables = [];
        }

        this._disposables.push({ proxied: resource, dispose: disposeFn });
    }

    /**
     * Creates a `Disposer` wrapper encapsulating the instance.
     * @returns An instance implementing the `Disposer` interface.
     */
    createDisposer(): IDisposer {
        return {
            addSubscription: s => this.addSubscription(s),
            autoDispose: r => this.autoDispose(r),
            autoDisposeProxy: (r, f) => this.autoDisposeProxy(r, f),
            disposeOf: r => this.disposeOf(r),
            getContext: () => this.getContext(),
            invokeOnDispose: f => this.invokeOnDispose(f),
            observeOnce: o => this.observeOnce(o),
            subscribeOnce: (o: any, n: any, e?: any, c?: any) =>
                // tslint:disable-next-line: no-unsafe-any
                this.subscribeOnce(o, n, e, c),
            subscribeTo: (o, n, e, c) => this.subscribeTo(o, n, e, c)
        };
    }

    /**
     * Disposes all resources maintained by the instance and removes them from its internal collection.
     */
    dispose() {
        if (this._disposables == undefined) return;
        this._disposables.forEach(d => {
            d.dispose();
        });
        this._context = undefined;
        this._disposables = undefined;
    }

    /**
     * If the specified resource is currently managed by the instance, removes it from the instance's internal collection
     * and disposes of it.
     *
     * @param resource The resource to dispose of.
     * @returns `true` if the resource was managed by the instance (and therefore disposed), otherwise returns `false`.
     */
    disposeOf(resource: any): boolean {
        if (this._disposables == undefined || resource == undefined) {
            return false;
        }
        const idx = this._disposables.findIndex(p => p.proxied === resource);
        if (idx < 0) return false;
        this._disposables[idx].dispose();
        this._disposables.splice(idx, 1);

        return true;
    }

    /**
     * Gets a reference to the instance's active `DisposableContext` instance.
     *
     * Note that a `DisposableContext` instance becomes stale upon the next invocation of the `dispose()` method.
     * If a fresh context is required following `dispose()`, then `getContext()`must be invoked again.
     */
    getContext(): IDisposableContext {
        if (this._context == undefined) {
            const context = createContext();
            this.autoDispose(context);
            this._context = context.shared;
        }

        return this._context;
    }

    /**
     * Registers a function to be invoked upon disposal. Note that this is the equivalent of invoking
     * `autoDisposeProxy(fn, fn)`, and therefore `fn` will be removed from the instance's internal collection
     * following invocation of `dispose()`.
     * @param fn The function to invoke upon disposal.
     */
    invokeOnDispose(fn: () => void): void {
        this.autoDisposeProxy(fn, fn);
    }

    /**
     * Returns an observable which proxies zero or one events raised by a source observable, and automatically detaches from
     * the source observable when the event has completed or dispose() has been invoked, whichever occurs first.
     * @param observable The source observable to subscribe to.
     * @returns An `Observable`.
     */
    observeOnce<T>(observable: Observable<T>): Observable<T> {
        const context = this.getContext();

        return observable.pipe(
            takeWhile(() => !context.isDisposed),
            // Ensure that any subscription completes upon the first event
            take(1),
            // Multicast the Observable so that it emits (replays) the buffered result to each new subscriber.
            shareReplay({ refCount: true, bufferSize: 1 })
        );
    }

    /**
     * Subscribes to an observable for zero or one events, and automatically unsubscribes when an event has completed or
     * dispose() has been invoked, whichever occurs first.
     *
     * @param observable The observable to subscribe to.
     * @param observer The observer which should handle the observable events.
     * @returns A `Subscription`.
     */
    subscribeOnce<T>(
        observable: Observable<T>,
        observer: PartialObserver<T>
    ): Subscription;
    /**
     * Subscribes to an observable for zero or one events, and automatically unsubscribes when an event has completed or
     * dispose() has been invoked, whichever occurs first.
     *
     * @param observable The observable to subscribe to.
     * @param next A handler for each value emitted by the observable.
     * @param error A optional error handler.
     * @param completed A optional completed handler.
     * @returns A `Subscription`.
     */
    subscribeOnce<T>(
        observable: Observable<T>,
        next: (value: T) => void,
        error?: (error: any) => void,
        completed?: () => void
    ): Subscription;
    /**
     * Subscribes to an observable for zero or one events, and automatically unsubscribes when an event has completed or
     * dispose() has been invoked, whichever occurs first.
     */
    subscribeOnce<T>(
        observable: Observable<T>,
        // tslint:disable-next-line: unified-signatures
        p1: PartialObserver<T> | ((value: T) => void),
        p2?: (error: any) => void,
        p3?: () => void
    ): Subscription;
    /**
     * Subscribes to an observable for zero or one events, and automatically unsubscribes when an event has completed or
     * dispose() has been invoked, whichever occurs first.
     */
    subscribeOnce<T>(
        observable: Observable<T>,
        p1: PartialObserver<T> | ((value: T) => void),
        p2?: (error: any) => void,
        p3?: () => void
    ): Subscription {
        // Important! Define sub here to prevent temporal reference error occurring in subscription body!
        // Do NOT declare const sub = subscribeOnce(observable, ...
        let sub: Subscription;
        sub = Async.subscribeOnce(
            this.observeOnce(observable),
            p1,
            e => {
                // If the operation errored asynchronously, sub will be removed from the internal array.
                // If the operation errored synchronously, sub will undefined and ignored.
                this.disposeOf(sub);
                if (p2 != undefined) p2(e);
            },
            () => {
                // If the operation errored asynchronously, sub will be removed from the internal array.
                // If the operation completed synchronously, sub will undefined and ignored.
                this.disposeOf(sub);
                if (p3 != undefined) p3();
            }
        );
        // If the subscription is already closed, it completed synchronously so we don't need to unsubscribe later.
        if (!sub.closed) {
            // We explictly add the subscription to disposables, and invoke disposeOf upon completion.
            // This ensures that the subscription will unsubscribe when dispose is invoked on the
            // `Disposables` instance, even in cases where the observable did not emit.
            this.addSubscription(sub);
        }

        return sub;
    }

    /**
     * Subscribes to an observable and automatically unsubscribes from it when dispose() has been invoked.
     *
     * @param observable The observable to subscribe to.
     * @param observer The observer which should handle the observable events.
     * @returns A `Subscription`.
     */
    subscribeTo<T>(
        observable: Observable<T>,
        observer: PartialObserver<T>
    ): Subscription;
    /**
     * Subscribes to an observable and automatically unsubscribes from it when dispose() has been invoked.
     *
     * @param observable The observable to subscribe to.
     * @param next A handler for each value emitted by the observable.
     * @param error A optional error handler.
     * @param completed A optional completed handler.
     * @returns A `Subscription`.
     */
    subscribeTo<T>(
        observable: Observable<T>,
        next: (value: T) => void,
        error?: (error: any) => void,
        completed?: () => void
    ): Subscription;
    /**
     * Subscribes to an observable and automatically unsubscribes from it when dispose() has been invoked.
     */
    subscribeTo<T>(
        observable: Observable<T>,
        // tslint:disable-next-line: unified-signatures
        p1: PartialObserver<T> | ((value: T) => void),
        p2?: (error: any) => void,
        p3?: () => void
    ): Subscription;
    /**
     * Subscribes to an observable and automatically unsubscribes from it when dispose() has been invoked.
     */
    subscribeTo<T>(
        observable: Observable<T>,
        p1: PartialObserver<T> | ((value: T) => void),
        p2?: (error: any) => void,
        p3?: () => void
    ): Subscription {
        const sub = observable.subscribe(<(value: T) => void>(<any>p1), p2, p3);
        this.addSubscription(sub);

        return sub;
    }
}

function createContext(): IInternalContext {
    let disposed = false;

    return {
        shared: {
            get isDisposed(): boolean {
                return disposed;
            }
        },
        dispose(): void {
            disposed = true;
        }
    };
}
